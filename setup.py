from setuptools import find_packages, setup
setup(name="esprit",
      version="0.2",
      description="Serialization down to structures JSON can handle",
      author="Daniel Moore",
      author_email='capnbearbossa@gmail.com',
      platforms=["any"],
      license="BSD",
      url="http://bitbucket.org/dan65536/esprit",
      packages=find_packages(),
      )

from __future__ import print_function
import types
import json

__all__ = [
    'register',
    'register_with_name',
    'helpers',
    'ser',
    'de_ser',
    'de_ser_params',
    'ser_result',
]

OPTIMIZE = True

# -- data state and helpers

_identity_fn = lambda _:_
_fundamental_types = { type(t):n for t,n in [ # -> {type:typename,...}
    ((),'tuple'),
    (1j,'complex'),
    ([],'list'),
    ({},'dict'),
    ('','str'),
    (0,'int'),
    (0.,'float'),
    (False,'bool'),
    (set(),'set'),
    (None, 'none')
]}

_rvs_fundamental_types = { v:k for k,v in _fundamental_types.items() }

_basic_type_info = { Type:(TypeName,_identity_fn)
                     for Type,TypeName in _fundamental_types.items() }

_basic_type_info[tuple] = ('tuple',lambda _:tuple(ser(o) for o in _))   # -> (a)
_basic_type_info[list]  = ('list',lambda _:tuple(ser(o) for o in _))     # -> (b)
_basic_type_info[dict]  = ('dict',lambda _:tuple((ser(a),ser(b)) for a,b in _.items()))   # -> (c)
_basic_type_info[set] = ('set',lambda _:tuple(ser(o) for o in _))       # -> (d)
_basic_type_info[complex] = ('complex',lambda o:tuple([o.real,o.imag]))

_approved_for_deserialize = { v[0] : None for t,v in _basic_type_info.items() }
_approved_for_deserialize ['complex'] = lambda _st: complex(_st[0],_st[1])

_approved_for_deserialize ['tuple'] = lambda _st:tuple(de_ser(o) for o in _st)  # -> this & (a)  needed for tup of ser-obj ?
_approved_for_deserialize ['list'] = lambda _st:list(de_ser(o) for o in _st)    # -> this & (b)  needed for list of ser-obj ?
_approved_for_deserialize ['dict'] = lambda _st:dict([[de_ser(a),de_ser(b)] for a,b in _st])    # -> this & (c)  needed for dict of ser-obj ?
_approved_for_deserialize ['set'] = lambda _st:set([de_ser(a) for a in _st])    # -> this & (d)  needed for dict of ser-obj  ?

_approved_for_deserialize ['none'] = lambda _st: None

_registered = {}
_rvs_registered = {}

def _register(cls,name=None):
    ser_name = (name or cls.__name__)
    _approved_for_deserialize[ ser_name ] = None
    _registered[ ser_name ] = cls
    _rvs_registered[cls.__name__] = ser_name
    return cls

# -- decorators --

def register(cls):
    return _register(cls)

class register_with_name:
    def __init__(self, name = None):
        self.name = name
    def __call__(self, cls):
        return _register(cls, name = self.name or cls.__name__)

def ser_result(func):
    def f(*args,**kw):
        res = (func(*args,**kw))
        res_ser = ser(res)
        return res_ser
    return f

class de_ser_params:
    def __init__(self,offsets=(),keywords=()):
        self.offsets = offsets
        self.keywords = keywords
    def __call__(self,func):
        def f(*args,**kw):
            new_args = [(de_ser(v) if n in self.offsets else v) for n,v in enumerate(args)]
            new_keywords = { k:(de_ser(v) if k in self.keywords else v) for k,v in kw.items() }
            return func(*new_args,**new_keywords)
        return f

def helpers(cls):
    def _state (self): return { k: ser(v) for k,v in self.__dict__.items() }
    def _prepare (self):
        for key,value in self.__dict__.copy().items():
            setattr(self,key,de_ser(value))
    cls._state = _state
    cls._prepare = _prepare
    return cls

# -- the api and internals --

def _get_basic_type_info( obj ):
    return _basic_type_info.get(type(obj), (None,None))

def ser (obj):
    ctor_ = False
    try:
        type_str = _fundamental_types.get(type(obj))           # class or instance object
        if type_str is None:
            type_str = _rvs_registered[obj.__class__.__name__]
        state_ = obj._state() if getattr(obj,'_state',None) else obj.__dict__
    except AttributeError:
        type_str, state_fn = _get_basic_type_info( obj )
        assert state_fn is not None,"Should not get here if all fund. types accounted for"
        if not(OPTIMIZE) or type(obj) in (tuple,list) or \
          state_fn is not _identity_fn:
            ctor_ = True
            state_ = state_fn( obj )
        else: return obj
    return type_str, state_, ctor_


def de_ser (_obj_repr):

  need_deserialize = ( type(_obj_repr) in (list,tuple) and
                       len(_obj_repr) and
                       _obj_repr[0] in _approved_for_deserialize)

  return _de_ser( _obj_repr ) if need_deserialize else _obj_repr

def _de_ser (dump):
    (type_str, state_ , ctor_) = dump
    builder = _approved_for_deserialize.get(type_str)
    if builder:
        return builder(state_)
    type_ = _rvs_fundamental_types.get(type_str,None) or _registered[type_str]    # was : eval(type_str)
    1;
    if ctor_:              # state used as argument (fundamental types only)
        return type_(state_)
    if getattr(type_,'__new__',None): # new-style classes allow initalizing to empty...
        t = type_.__new__(type_)
    else:
        t = type_()              # old-style classes must be able to construct without args
    t.__dict__.update(state_)
    if getattr(type_,'_prepare',None): t._prepare()
    return t

def test(*args):

    if args == ('.',):

        # -- test 1 --

        for o in [None,set((3,4)), 1+1j, (3,4), [3,4]
        ]:
            x = ser(o)
            print( x )
            print( '\t',de_ser( x ))
        exit()

    @register_with_name('__Bag_') # --> specify a serializing name for the class
    class Bag:
        def __repr__(self):
            return '<{} {}>'.format(self.__class__.__name__, self.__dict__)

    @register              # --> register using the class' __name__ as its serializing name
    class ClsObj (object):
        def __repr__(self):
            return '<{} {}>'.format(self.__class__.__name__, self.__dict__)
        def __init__(self,x='hello world'):
            self.x = x
        def _state (self): return { 'x': ser(self.x) }
        def _prepare (self):
            self.x = de_ser( self.x )

    @helpers
    @register_with_name('_serialize2_Base')
    class Base (object):
        def __repr__(self):
            return '<{} {}>'.format(self.__class__.__name__, self.__dict__)
        def __init__(self,**k):
            self.__dict__.update(k)

    # -- test 2 --

    bag = Bag(); bag.x = [ 'structured', 'data', None,{1,2}, (), [], {} ]

    for obj in ( bag,
                 ClsObj('a'),
                 set((3,)),
                 1,
                 2j, (),[],3.,{}
               ):
        info = ser( obj )
        print(info)
        y = de_ser( info )
        print("\t",y)

    # -- test 3 -- fancy stuff

    b = Base(y = 3, n = None, z = 3.0j, w = ClsObj(3))

    print('\n''input to be serialized -->',b)
    bser = ser(b)
    jbser = json.dumps(bser,indent = 4)

    print('python -->',bser)
    print('json   -->',jbser)

    b_rehydrate = de_ser (bser)

    print('==>',b_rehydrate)
    import cmath
    print('calculated square root of complex member: ' , cmath.sqrt( b_rehydrate.z ))
    print('type of class object member: ' , type( b_rehydrate.w ))


if __name__ == '__main__':

    import sys

    @helpers
    @register
    class P(object):                      # -- Don't use old-style classes as they don't have __new__().
                                          #    Inherit from object so that the __new__ can be used to
                                          #    construct blank objects (i.e. with self.__dict__ == {}) ready
                                          #    for deserialization (i.e. dict update with the stored state).
                                          #    We permit old-style classes [just to be nice :)], but if you
                                          #    serialize/de-serialize them you must provide a zero-arg 
                                          #    constructor or equivalent that simulates a blank object.
      def __init__(self,i): self.i=i
      def __repr__(self): return 'P(%r)'%self.i

    for a in [ (P(3),), [P(4),], set([P(5)]), dict( a=3, b=[(P(4),), [P(3)]]),
              ]:
        x = ser(a)
        y = de_ser(x)
        print (
               " a = {a}\n"
               " x = {x}\n"
               " y = {y}\n ---\n".format(**locals())
        )

    #if sys.argv[1:]==['e']: exit()

    test(*sys.argv[1:])

    @ser_result
    @de_ser_params(keywords=['ser_arg'],offsets=[0])
    def f(x,y,ser_arg=None):
        from_inp = dict(x_y=(x,y), ser_arg = ser_arg)
        print ("internally to f:",from_inp)
        return from_inp
    print( 'output of f: ',f(ser(P('a')), # x
                             34,          # y
                             ser_arg=ser(P('k'))) )
